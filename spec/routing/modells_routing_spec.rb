require "rails_helper"

RSpec.describe ModellsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/modells").to route_to("modells#index")
    end

    it "routes to #new" do
      expect(:get => "/modells/new").to route_to("modells#new")
    end

    it "routes to #show" do
      expect(:get => "/modells/1").to route_to("modells#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/modells/1/edit").to route_to("modells#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/modells").to route_to("modells#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/modells/1").to route_to("modells#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/modells/1").to route_to("modells#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/modells/1").to route_to("modells#destroy", :id => "1")
    end

  end
end
