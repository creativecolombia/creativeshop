class Offer < ApplicationRecord
  include PublicActivity::Model
  #extend FriendlyId

  #has_paper_trail

  include PgSearch
  pg_search_scope :quick_search , against: [:machinenumber, :serialnumber, :agedate]
  include Payola::Sellable


  tracked owner:  -> (controller, model ) {controller && controller.current_user}
  mount_uploaders :offerpics, ProductUploader
  #friendly_id :slug_candidates, use: [:slugged, :finders]
  acts_as_paranoid
  #acts_as_taggable # Alias for acts_as_taggable_on :tags
  #acts_as_taggable_on :tags
  acts_as_commentable
  #searchkick

  belongs_to :condition
  belongs_to :incoterm
  belongs_to :modell
=begin
  def slug_candidates
      [
          [ :id],
      #[Modell.friendly_id.find(modell_id).name, :id],

      ]
  end
  def should_generate_new_friendly_id?
    id_changed? || super
  end
=end
end
