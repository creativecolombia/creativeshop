 module ApplicationHelper


  def title(page_title)
    content_for :title, "#{app.name} - #{page_title.to_s}"
  end

  #TODO reduce code inside title
  def header(page_title)
    content_for :headertitle, page_title.to_s
  end

  #green grey yellow red blue pink light dark
  def headercolor(headerbg)
    content_for :headercolor, "bg-#{headerbg.to_s}"
  end

  def gravatar_for(user, size = 30, title = user.name)
    image_tag gravatar_image_url(user.email, size: size), title: title, class: 'img-rounded'
  end


  def app
    @app=App.first
  end

  def profile
    @profile=current_user.profile
  end

  def current_controller(current)
    controller_path().split('/').first==current
  end

  def categories_menu
    #Category.where.not(id: 1)
    Category.find_by_main(true).children
  end

  def brands_menu
    Brand.all
  end


  #isCurrent helper
  def admin?
    controller.class.name.split("::").first=="Admin"
  end
  def shop?
    controller.class.name.split("::").first=="Shop"
  end
  def panel?
    controller.class.name.split("::").first=="Panel"
  end


  #nested helper
  def nested_categories(categories)
    categories.map do |category, sub_category|
      render(category) + content_tag(:div, nested_categories(sub_category), :class => "nested_messages")
    end.join.html_safe
  end
  def nested_categories_index(categories)
    categories.map do |category, sub_category|
      render(:partial => 'shop/categories/categories',locals: {category: category}) + content_tag(:div, nested_categories_index(sub_category), :class => "nested_messages")
    end.join.html_safe
  end



  #Languages helper
  def languages(obj=nil)
    content_for(:switch_locale) do
      I18n.available_locales.each do |locale|
        I18n.with_locale(locale) do
          concat(
              if obj
                content_tag(:li, (link_to locale, url_for([:shop, obj]) ))
              else
                content_tag(:li, (link_to locale, url_for(locale: locale.to_s) ))
              end
          )
        end
      end
    end
  end

  def languages_admin(obj=nil)
    content_for(:switch_locale) do
      I18n.available_locales.each do |locale|
        I18n.with_locale(locale) do
          concat(
              if obj
                content_tag(:li, (link_to locale, url_for([:admin, obj]) ))
              else
                content_tag(:li, (link_to locale, url_for(locale: locale.to_s) ))
              end
          )
        end
      end
    end
  end

  def languages_panel(obj=nil)
    content_for(:switch_locale) do
      I18n.available_locales.each do |locale|
        I18n.with_locale(locale) do
          concat(
              if obj
                content_tag(:li, (link_to locale, url_for([:panel, obj])), :class => "hidden-xs")
              else
                content_tag(:li, (link_to locale, url_for(locale: locale.to_s)), :class => "hidden-xs")
              end
          )
        end
      end
    end
  end
end
