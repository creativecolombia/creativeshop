class Api::V1::CategoriesController < ApplicationController
  #respond_to :json
  before_action :set_modell, only: [:show, :edit, :update, :destroy]
  #skip_before_action :authenticate_user!, only: [:index,:show]

  # GET /modells
  def index
    @modell = Modell.all
    render json: @modell
  end


  # GET /modells/1
  def show
    render json: @modell
  end

  # POST /modells
  def create
    @modell = Modell.new(modell_params)
   if @modell.save
      render json: :created, location: @modell
    else
      render json: @modell.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /modells/1
  def update
      if @modell.update(modell_params)
        render json: :ok, location: @modell
      else
        render json: @modell.errors, status: :unprocessable_entity
      end
    end
  end

  # DELETE /modells/1
  def destroy
    @modell.destroy
    render json: :no_content
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_modell
    @modell = Modell.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def modell_params
    params.require(:modell).permit(:name, :year, {factorypics: []}, :enabled, :brand_id)
  end
