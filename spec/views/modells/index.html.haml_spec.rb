require 'rails_helper'

RSpec.describe "modells/index", type: :view do
  before(:each) do
    assign(:modells, [
      Modell.create!(
        :name => "Name",
        :factorypics => "",
        :enabled => false,
        :brand => nil
      ),
      Modell.create!(
        :name => "Name",
        :factorypics => "",
        :enabled => false,
        :brand => nil
      )
    ])
  end

  it "renders a list of modells" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
