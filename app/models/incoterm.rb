class Incoterm < ApplicationRecord
  include PublicActivity::Model
  extend FriendlyId

  tracked owner:  -> (controller, model ) {controller && controller.current_user}
  mount_uploader :picture, AvatarUploader
  friendly_id :slug_candidates, use: [:slugged, :finders]
  acts_as_paranoid

  def slug_candidates
    [
        :smalltag,
        [:smalltag, :name]
    ]
  end
  def should_generate_new_friendly_id?
    smalltag_changed? || super
  end
end
