class Profile < ApplicationRecord
  include PublicActivity::Model
  extend FriendlyId

  tracked owner:  -> (controller, model ) {controller && controller.current_user}
  mount_uploader :picture, AvatarUploader
  friendly_id :slug_candidates, use: [:slugged, :finders]
  acts_as_paranoid
  #searchkick

  belongs_to :user
  belongs_to :admin

  def slug_candidates
    [
        :firstname,
        [:firstname, :lastname]
    ]
  end
  def should_generate_new_friendly_id?
    firstname_changed? || super
  end
end
