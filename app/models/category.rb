class Category < ApplicationRecord
  include PublicActivity::Model
  translates :name,:slug ,fallbacks_for_empty_translations: true
  extend FriendlyId

  friendly_id :slug_candidates, use: [:slugged, :finders,:globalize]
  has_ancestry
  acts_as_paranoid
  #acts_as_taggable_on :tags
  #searchkick
  #tracked owner:  -> (controller, model ) {controller && controller.current_user}

  has_many :categorymodels
  has_many :modells, through: :categorymodels

  def slug_candidates
    if !self.parent.nil?
      [
          parent.name,:name,
          [parent.name,:name, :id]
      ]
    else
      [
          :name,
          [:name, :id]
      ]
    end

  end

  def subproducts
    subtree.flat_map{|x| x.modells}.uniq.sort  # or use descendants instead of children
  end


end
