class CreateBrands < ActiveRecord::Migration[5.0]
  def change
    create_table :brands do |t|
      t.string :name
      t.string :sdescription
      t.string :description
      t.string :logo
      t.string :metatitle
      t.boolean :enable

      t.timestamps
    end
  end
end
