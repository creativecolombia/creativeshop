class Shop::BrandsController < ShopController
  before_action :set_brand, only: [:show, :edit, :update, :destroy]
  skip_before_action :authenticate_user!, only: [:index,:show],raise: false


  # GET /brands/1
  # GET /brands/1.json
  def show
    @products=@brand.modells
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_brand
      @brand = Brand.friendly.find(params[:id])
    end

end
