class AddSlugtoOffer < ActiveRecord::Migration[5.0]
  def change
    add_column :offers, :slug, :string
    add_index  :offers, :slug
  end
end
