json.extract! brand, :id, :name, :sdescription, :description, :logo, :metatitle, :enable, :created_at, :updated_at
json.url panel_brand_url(brand, format: :json)