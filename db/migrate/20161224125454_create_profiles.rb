class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.string :firstname
      t.string :lastname
      t.string :location
      t.string :status
      t.string :picture

      t.timestamps
    end
  end
end
