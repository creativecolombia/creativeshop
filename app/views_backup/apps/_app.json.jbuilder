json.extract! app, :id, :name, :description, :avatar, :allownew, :created_at, :updated_at
json.url app_url(app, format: :json)