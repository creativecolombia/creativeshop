require 'rails_helper'

RSpec.describe "offers/index", type: :view do
  before(:each) do
    assign(:offers, [
      Offer.create!(
        :serialnumber => "Serialnumber",
        :machinenumber => "Machinenumber",
        :condition => nil,
        :incoterm => nil,
        :images => "",
        :description => "Description",
        :avability => false,
        :price_cents => "Price Cents",
        :modell => nil,
        :enabled => false
      ),
      Offer.create!(
        :serialnumber => "Serialnumber",
        :machinenumber => "Machinenumber",
        :condition => nil,
        :incoterm => nil,
        :images => "",
        :description => "Description",
        :avability => false,
        :price_cents => "Price Cents",
        :modell => nil,
        :enabled => false
      )
    ])
  end

  it "renders a list of offers" do
    render
    assert_select "tr>td", :text => "Serialnumber".to_s, :count => 2
    assert_select "tr>td", :text => "Machinenumber".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Price Cents".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
