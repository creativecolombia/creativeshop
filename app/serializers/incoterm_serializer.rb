class IncotermSerializer < ActiveModel::Serializer
  attributes :id, :smalltag, :name, :description
end
