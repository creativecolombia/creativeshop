require 'rails_helper'

RSpec.describe "profiles/new", type: :view do
  before(:each) do
    assign(:profile, Profile.new(
      :firstname => "MyString",
      :lastname => "MyString",
      :location => "MyString",
      :status => "MyString",
      :picture => "MyString"
    ))
  end

  it "renders new profile form" do
    render

    assert_select "form[action=?][method=?]", profiles_path, "post" do

      assert_select "input#profile_firstname[name=?]", "profile[firstname]"

      assert_select "input#profile_lastname[name=?]", "profile[lastname]"

      assert_select "input#profile_location[name=?]", "profile[location]"

      assert_select "input#profile_status[name=?]", "profile[status]"

      assert_select "input#profile_picture[name=?]", "profile[picture]"
    end
  end
end
