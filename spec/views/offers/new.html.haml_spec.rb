require 'rails_helper'

RSpec.describe "offers/new", type: :view do
  before(:each) do
    assign(:offer, Offer.new(
      :serialnumber => "MyString",
      :machinenumber => "MyString",
      :condition => nil,
      :incoterm => nil,
      :images => "",
      :description => "MyString",
      :avability => false,
      :price_cents => "MyString",
      :modell => nil,
      :enabled => false
    ))
  end

  it "renders new offer form" do
    render

    assert_select "form[action=?][method=?]", offers_path, "post" do

      assert_select "input#offer_serialnumber[name=?]", "offer[serialnumber]"

      assert_select "input#offer_machinenumber[name=?]", "offer[machinenumber]"

      assert_select "input#offer_condition_id[name=?]", "offer[condition_id]"

      assert_select "input#offer_incoterm_id[name=?]", "offer[incoterm_id]"

      assert_select "input#offer_images[name=?]", "offer[images]"

      assert_select "input#offer_description[name=?]", "offer[description]"

      assert_select "input#offer_avability[name=?]", "offer[avability]"

      assert_select "input#offer_price_cents[name=?]", "offer[price_cents]"

      assert_select "input#offer_modell_id[name=?]", "offer[modell_id]"

      assert_select "input#offer_enabled[name=?]", "offer[enabled]"
    end
  end
end
