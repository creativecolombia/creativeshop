require 'rails_helper'

RSpec.describe "modells/new", type: :view do
  before(:each) do
    assign(:modell, Modell.new(
      :name => "MyString",
      :factorypics => "",
      :enabled => false,
      :brand => nil
    ))
  end

  it "renders new modell form" do
    render

    assert_select "form[action=?][method=?]", modells_path, "post" do

      assert_select "input#modell_name[name=?]", "modell[name]"

      assert_select "input#modell_factorypics[name=?]", "modell[factorypics]"

      assert_select "input#modell_enabled[name=?]", "modell[enabled]"

      assert_select "input#modell_brand_id[name=?]", "modell[brand_id]"
    end
  end
end
