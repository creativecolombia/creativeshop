require 'application_responder'
class ApplicationController < ActionController::Base
  include PublicActivity::StoreController
  include ActionController::Serialization
  include ApplicationHelper

  self.responder = ApplicationResponder
   respond_to :html,:json,:js

  rescue_from ActiveRecord::RecordNotFound do
    flash[:warning] = 'Resource not found.'
    redirect_back_or root_path
  end

  before_action :set_locale
  protect_from_forgery with: :exception
  after_action :track_action

  def redirect_back_or(path)
    redirect_to request.referer || path
  end

  #Locale I18n
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
  def default_url_options(options={})
    { locale: I18n.locale }
  end


  protected
  def track_action
    #TODO retrack
    #ahoy.track "Viewed #{controller_name}##{action_name}"
  end



end
