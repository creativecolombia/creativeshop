require 'rails_helper'

RSpec.describe "brands/show", type: :view do
  before(:each) do
    @brand = assign(:brand, Brand.create!(
      :name => "Name",
      :sdescription => "Sdescription",
      :description => "Description",
      :logo => "Logo",
      :metatitle => "Metatitle",
      :enable => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Sdescription/)
    expect(rendered).to match(/Description/)
    expect(rendered).to match(/Logo/)
    expect(rendered).to match(/Metatitle/)
    expect(rendered).to match(/false/)
  end
end
