class RegistrationsController < Devise::RegistrationsController
  def create
    super
    Profile.create(user: current_user)
  end
end