class Admin::CategorymodelsController < AdminController
  before_action :set_categorymodel, only: [:show, :edit, :update, :destroy]

  # GET /categorymodels
  # GET /categorymodels.json
  def index
    @categorymodels = Categorymodel.all
  end

  # GET /categorymodels/1
  # GET /categorymodels/1.json
  def show
  end

  # GET /categorymodels/new
  def new
    @categorymodel = Categorymodel.new
  end

  # GET /categorymodels/1/edit
  def edit
  end

  # POST /categorymodels
  # POST /categorymodels.json
  def create
    @categorymodel = Categorymodel.new(categorymodel_params)

    respond_to do |format|
      if @categorymodel.save
        format.html { redirect_to admin_categorymodel_path(@categorymodel), notice: 'Categorymodel was successfully created.' }
        format.json { render :show, status: :created, location: @categorymodel }
      else
        format.html { render :new }
        format.json { render json: @categorymodel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categorymodels/1
  # PATCH/PUT /categorymodels/1.json
  def update
    respond_to do |format|
      if @categorymodel.update(categorymodel_params)
        format.html { redirect_to admin_categorymodel_path(@categorymodel), notice: 'Categorymodel was successfully updated.' }
        format.json { render :show, status: :ok, location: @categorymodel }
      else
        format.html { render :edit }
        format.json { render json: @categorymodel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categorymodels/1
  # DELETE /categorymodels/1.json
  def destroy
    @categorymodel.destroy
    respond_to do |format|
      format.html { redirect_to admin_categorymodels_url, notice: 'Categorymodel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_categorymodel
    @categorymodel = Categorymodel.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def categorymodel_params
    params.require(:categorymodel).permit(:category_id, :modell_id)
  end
end
