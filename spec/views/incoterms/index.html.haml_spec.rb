require 'rails_helper'

RSpec.describe "incoterms/index", type: :view do
  before(:each) do
    assign(:incoterms, [
      Incoterm.create!(
        :smalltag => "Smalltag",
        :name => "Name",
        :description => "Description"
      ),
      Incoterm.create!(
        :smalltag => "Smalltag",
        :name => "Name",
        :description => "Description"
      )
    ])
  end

  it "renders a list of incoterms" do
    render
    assert_select "tr>td", :text => "Smalltag".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
