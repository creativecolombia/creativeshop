require 'rails_helper'

RSpec.describe "brands/edit", type: :view do
  before(:each) do
    @brand = assign(:brand, Brand.create!(
      :name => "MyString",
      :sdescription => "MyString",
      :description => "MyString",
      :logo => "MyString",
      :metatitle => "MyString",
      :enable => false
    ))
  end

  it "renders the edit brand form" do
    render

    assert_select "form[action=?][method=?]", brand_path(@brand), "post" do

      assert_select "input#brand_name[name=?]", "brand[name]"

      assert_select "input#brand_sdescription[name=?]", "brand[sdescription]"

      assert_select "input#brand_description[name=?]", "brand[description]"

      assert_select "input#brand_logo[name=?]", "brand[logo]"

      assert_select "input#brand_metatitle[name=?]", "brand[metatitle]"

      assert_select "input#brand_enable[name=?]", "brand[enable]"
    end
  end
end
