require 'rails_helper'

RSpec.describe "modells/show", type: :view do
  before(:each) do
    @modell = assign(:modell, Modell.create!(
      :name => "Name",
      :factorypics => "",
      :enabled => false,
      :brand => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(//)
    expect(rendered).to match(/false/)
    expect(rendered).to match(//)
  end
end
