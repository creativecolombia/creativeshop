class CreateIncoterms < ActiveRecord::Migration[5.0]
  def change
    create_table :incoterms do |t|
      t.string :smalltag
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
