class User < ApplicationRecord

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  acts_as_messageable
  #acts_as_tagger
  acts_as_follower
  acts_as_followable
  acts_as_votable
  #has_merit
  #searchkick

  has_many :activities
  has_one :profile

  def name
    email
  end

  def mailboxer_email(object)
    email
  end
end
