class AdminregistrationsController < Devise::RegistrationsController
  def create
    super
    Profile.create(admin: current_admin)
  end
end