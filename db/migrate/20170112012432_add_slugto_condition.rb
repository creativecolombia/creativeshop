class AddSlugtoCondition < ActiveRecord::Migration[5.0]
  def change
    add_column :conditions, :slug, :string
    add_index  :conditions, :slug
  end
end
