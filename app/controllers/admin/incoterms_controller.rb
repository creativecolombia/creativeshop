class Admin::IncotermsController < AdminController
  before_action :set_incoterm, only: [:show, :edit, :update, :destroy]

  # GET /incoterms
  # GET /incoterms.json
  def index
    @incoterms = Incoterm.all
  end

  # GET /incoterms/1
  # GET /incoterms/1.json
  def show
  end

  # GET /incoterms/new
  def new
    @incoterm = Incoterm.new
  end

  # GET /incoterms/1/edit
  def edit
  end

  # POST /incoterms
  # POST /incoterms.json
  def create
    @incoterm = Incoterm.new(incoterm_params)

    respond_to do |format|
      if @incoterm.save
        format.html { redirect_to admin_incoterm_path(@incoterm), notice: 'Incoterm was successfully created.' }
        format.json { render :show, status: :created, location: @incoterm }
      else
        format.html { render :new }
        format.json { render json: @incoterm.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /incoterms/1
  # PATCH/PUT /incoterms/1.json
  def update
    respond_to do |format|
      if @incoterm.update(incoterm_params)
        format.html { redirect_to admin_incoterm_path(@incoterm), notice: 'Incoterm was successfully updated.' }
        format.json { render :show, status: :ok, location: @incoterm }
      else
        format.html { render :edit }
        format.json { render json: @incoterm.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /incoterms/1
  # DELETE /incoterms/1.json
  def destroy
    @incoterm.destroy
    respond_to do |format|
      format.html { redirect_to admin_incoterms_url, notice: 'Incoterm was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_incoterm
    @incoterm = Incoterm.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def incoterm_params
    params.require(:incoterm).permit(:smalltag, :name, :description)
  end
end
