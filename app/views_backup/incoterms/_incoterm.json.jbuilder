json.extract! incoterm, :id, :smalltag, :name, :description, :created_at, :updated_at
json.url incoterm_url(incoterm, format: :json)