class Addimagestooffers < ActiveRecord::Migration[5.0]
  def change
    remove_column :offers, :images
    add_column :offers, :offerpics, :json
  end
end
