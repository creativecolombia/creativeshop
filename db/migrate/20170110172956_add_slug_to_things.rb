class AddSlugToThings < ActiveRecord::Migration[5.0]
  def change
    add_column :apps, :slug, :string
    add_column :brands, :slug, :string
    add_column :categories, :slug, :string
    add_column :modells, :slug, :string
    add_column :profiles, :slug, :string

    add_index  :apps, :slug
    add_index  :brands, :slug
    add_index  :categories, :slug
    add_index  :modells, :slug
    add_index  :profiles, :slug

  end
end
