require 'rails_helper'

RSpec.describe "apps/new", type: :view do
  before(:each) do
    assign(:app, App.new(
      :name => "MyString",
      :description => "MyString",
      :avatar => "MyString",
      :allownew => false
    ))
  end

  it "renders new app form" do
    render

    assert_select "form[action=?][method=?]", apps_path, "post" do

      assert_select "input#app_name[name=?]", "app[name]"

      assert_select "input#app_description[name=?]", "app[description]"

      assert_select "input#app_avatar[name=?]", "app[avatar]"

      assert_select "input#app_allownew[name=?]", "app[allownew]"
    end
  end
end
