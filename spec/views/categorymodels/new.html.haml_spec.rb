require 'rails_helper'

RSpec.describe "categorymodels/new", type: :view do
  before(:each) do
    assign(:categorymodel, Categorymodel.new(
      :show => nil,
      :modell => nil
    ))
  end

  it "renders new categorymodel form" do
    render

    assert_select "form[action=?][method=?]", categorymodels_path, "post" do

      assert_select "input#categorymodel_category_id[name=?]", "categorymodel[category_id]"

      assert_select "input#categorymodel_modell_id[name=?]", "categorymodel[modell_id]"
    end
  end
end
