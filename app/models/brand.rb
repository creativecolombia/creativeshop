class Brand < ApplicationRecord
  include PublicActivity::Model
  extend FriendlyId

  tracked owner:  -> (controller, model ) {controller && controller.current_user}
  mount_uploader :logo, AvatarUploader
  friendly_id :slug_candidates, use: [:slugged, :finders]
  acts_as_paranoid
  #acts_as_taggable # Alias for acts_as_taggable_on :tags
  #acts_as_taggable_on :tags
  acts_as_commentable
  acts_as_votable
  #searchkick

  has_many :modells

  #acts_as_taggable # Alias for acts_as_taggable_on :tags
  #acts_as_taggable_on :skills, :interests TODO create tag field.

  def slug_candidates
    [
        :name,
        [:name, :id]
    ]
  end
  def should_generate_new_friendly_id?
    name_changed? || super
  end
end
