class Modell < ApplicationRecord
  #include PublicActivity::Model
  extend FriendlyId
  #tracked owner:  -> (controller, model ) {controller && controller.current_user}
  mount_uploaders :factorypics, ProductUploader
  friendly_id :slug_candidates, use: [:slugged, :finders]
  #acts_as_paranoid
  #acts_as_taggable # Alias for acts_as_taggable_on :tags
  #acts_as_taggable_on :tags
  acts_as_votable
  #searchkick
  belongs_to :brand
  has_many :categorymodels
  has_many :categories, through: :categorymodels
  has_many :offers
  #validates :brand, presence: true

  def slug_candidates
    [
        :name,
        [:name, :id]
    ]
  end
  def should_generate_new_friendly_id?
    name_changed? || super
  end


end
