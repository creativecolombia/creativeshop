class Api::V1::CategoriesController < ApplicationController
  #respond_to :json
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  #skip_before_action :authenticate_user!, only: [:index,:show]

  # GET /categories
  def index
    @categories = Category.all
    render json: @categories
  end


  # GET /categories/1
  def show
    render json: @category
  end

  # POST /categories
  def create
    @category = Category.new(category_params)
   if @category.save
      render json: :created, location: @category
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /categories/1
  def update
      if @category.update(category_params)
        render json: :ok, location: @category
      else
        render json: @category.errors, status: :unprocessable_entity
      end
    end
  end

  # DELETE /categories/1
  def destroy
    @category.destroy
    render json: :no_content
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_category
    @category = Category.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def category_params
    params.require(:show).permit(:name, :description, :main, :parent_id)
  end
