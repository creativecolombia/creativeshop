json.extract! categorymodel, :id, :category_id, :modell_id, :created_at, :updated_at
json.url admin_categorymodel_url(categorymodel, format: :json)