class Addtagstomodels < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :tags,:string
    add_column :offers, :tags,:string
    add_column :modells, :tags,:string
    add_column :brands, :tags,:string
  end
end
