class Shop::ProductsController < ShopController
  before_action :set_product, only: [:show]
  skip_before_action :authenticate_user!, only: [:index,:show],raise: false

  def index
    @categories=Category.find_by_main(true)
    @brands=Brand.all
    if params[:tag]
      @products = Modell.tagged_with(params[:tag])
    else
      @products = Modell.all
    end
  end

  def show
  end
end

private
# Use callbacks to share common setup or constraints between actions.
def set_product
  @product = Modell.friendly.find(params[:id])
end