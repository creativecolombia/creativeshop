class CreateOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :offers do |t|
      t.string :serialnumber
      t.string :machinenumber
      t.date :agedate
      t.references :condition, foreign_key: true
      t.references :incoterm, foreign_key: true
      t.json :images
      t.string :description
      t.boolean :avability
      t.string :price_cents
      t.references :modell, foreign_key: true
      t.boolean :enabled

      t.timestamps
    end
  end
end
