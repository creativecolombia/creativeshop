class Admin::ModellsController < AdminController
  before_action :set_modell, only: [:show, :edit, :update, :destroy]

  # GET /modells
  # GET /modells.json
  def index
    if params[:tag]
      @modells = Modell.tagged_with(params[:tag])
    else
      @modells = Modell.all
    end
  end

  # GET /modells/1
  # GET /modells/1.json
  def show
  end

  # GET /modells/new
  def new
    @chosen_category = Category.find_by(id: params[:to].to_i) if params[:to]
    @modell = Modell.new
  end

  # GET /modells/1/edit
  def edit
  end

  def tag_cloud
    @tags = Modell.tag_counts_on(:tags)
  end

  # POST /modells
  # POST /modells.json
  def create
    @modell = Modell.new(modell_params)

    respond_to do |format|
      if @modell.save
        @categories = Category.where(id: params['categories'])
        @categories.each do |cat|
          @modell.categorymodels.create(show: cat)
        end
        format.html { redirect_to admin_modell_path(@modell), notice: 'Modell was successfully created.' }
        format.json { render :show, status: :created, location: @modell }
      else
        format.html { render :new }
        format.json { render json: @modell.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /modells/1
  # PATCH/PUT /modells/1.json
  def update
    respond_to do |format|
      #TODO Improve method
      @modell.categorymodels.delete_all
      @categories = Category.where(id: params['categories'])
      @categories.each do |cat|
        @modell.categorymodels.create(show: cat)
      end
      #@modell.categorymodels.create(@categories)
      if @modell.update(modell_params)
        format.html { redirect_to admin_modell_path(@modell), notice: 'Modell was successfully updated.' }
        format.json { render :show, status: :ok, location: @modell }
      else
        format.html { render :edit }
        format.json { render json: @modell.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /modells/1
  # DELETE /modells/1.json
  def destroy
    @modell.destroy
    respond_to do |format|
      format.html { redirect_to admin_modells_url, notice: 'Modell was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_modell
    @modell = Modell.friendly.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def modell_params
    params.require(:modell).permit(:name, :year, {factorypics: []},:tag_list,:enabled, :brand_id,:country_code)
  end
end
