require 'rails_helper'

RSpec.describe "apps/index", type: :view do
  before(:each) do
    assign(:apps, [
      App.create!(
        :name => "Name",
        :description => "Description",
        :avatar => "Avatar",
        :allownew => false
      ),
      App.create!(
        :name => "Name",
        :description => "Description",
        :avatar => "Avatar",
        :allownew => false
      )
    ])
  end

  it "renders a list of apps" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Avatar".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
