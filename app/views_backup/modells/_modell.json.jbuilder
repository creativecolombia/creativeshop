json.extract! modell, :id, :name, :year, :factorypics, :enabled, :brand_id, :created_at, :updated_at
json.url modell_url(modell, format: :json)