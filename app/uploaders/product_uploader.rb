class ProductUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  include CarrierWave::ImageOptimizer
  #process :quality => 100


  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end


  version :thumb do
    process :resize_to_limit => [213, 200]
    process :watermark
    process :convert => 'jpg'
    process optimize:  [{ quiet: true }]#[{ quality: 100 }]

  end
  version :index do
    process :resize_to_limit => [500, 500]
    process :watermark
    process :convert => 'jpg'
    process optimize:  [{ quiet: true }]#[{ quality: 100 }]

  end



  def watermark
    #second_image = MiniMagick::Image.open("#{Rails.root}/app/assets/images/logo.png")
=begin
      result = img.composite(second_image) do |c|
      c.compose 'Over'
      c.gravity 'Southeast'
      c.gravity 'center'

      end
=end
    manipulate! do |img|

      result= img.combine_options do |c|
        c.font "#{Rails.root}/app/assets/fonts/pontiac.otf"
        c.pointsize '10'

        #north
        c.fill 'grey'
        c.gravity 'Northwest'
        c.draw 'text 0,15 "Printerspart"'

        #center

        c.fill 'white'
        c.gravity 'Center'
        c.draw 'text 0,0 "Printerspart"'

        #south

        c.fill 'grey'
        c.gravity 'Southeast'
        c.draw 'text 15,0 "Printerspart"'
      end
      result
    end
  end


end
