class Addparanoidtomodels < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :deleted_at, :datetime
    add_index :brands, :deleted_at
    add_column :categories, :deleted_at, :datetime
    add_index :categories, :deleted_at
    add_column :categorymodels, :deleted_at, :datetime
    add_index :categorymodels, :deleted_at
    add_column :conditions, :deleted_at, :datetime
    add_index :conditions, :deleted_at
    add_column :incoterms, :deleted_at, :datetime
    add_index :incoterms, :deleted_at
    add_column :modells, :deleted_at, :datetime
    add_index :modells, :deleted_at
    add_column :offers, :deleted_at, :datetime
    add_index :offers, :deleted_at
    add_column :profiles, :deleted_at, :datetime
    add_index :profiles, :deleted_at
  end
end
