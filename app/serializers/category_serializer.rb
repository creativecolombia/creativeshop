class CategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :main, :ancestors, :kids

  def ancestors
    object.ancestor_ids unless object.is_root?
  end

  def kids
    object.descendant_ids if object.has_children?
  end

end
