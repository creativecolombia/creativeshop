class AddParentandPositionstoCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :position, :integer


    #Category.order(:updated_at).each.with_index(1) do |todo_item, index|
    #  todo_item.update_column :position, index
    #end

    add_column :categories, :parent_id, :integer
    add_index :categories, :parent_id

  end
end
