class CreateApps < ActiveRecord::Migration[5.0]
  def change
    create_table :apps do |t|
      t.string :name
      t.string :description
      t.string :avatar
      t.boolean :allownew

      t.timestamps
    end
  end
end
