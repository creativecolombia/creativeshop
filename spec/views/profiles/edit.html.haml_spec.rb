require 'rails_helper'

RSpec.describe "profiles/edit", type: :view do
  before(:each) do
    @profile = assign(:profile, Profile.create!(
      :firstname => "MyString",
      :lastname => "MyString",
      :location => "MyString",
      :status => "MyString",
      :picture => "MyString"
    ))
  end

  it "renders the edit profile form" do
    render

    assert_select "form[action=?][method=?]", profile_path(@profile), "post" do

      assert_select "input#profile_firstname[name=?]", "profile[firstname]"

      assert_select "input#profile_lastname[name=?]", "profile[lastname]"

      assert_select "input#profile_location[name=?]", "profile[location]"

      assert_select "input#profile_status[name=?]", "profile[status]"

      assert_select "input#profile_picture[name=?]", "profile[picture]"
    end
  end
end
