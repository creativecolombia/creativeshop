class Condition < ApplicationRecord
  include PublicActivity::Model
  extend FriendlyId
  tracked owner:  -> (controller, model ) {controller && controller.current_user}
  friendly_id :name, use: [:slugged, :finders]
  acts_as_paranoid

  def should_generate_new_friendly_id?
    name_changed? || super
  end
end
