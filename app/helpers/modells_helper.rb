module ModellsHelper
  include ActsAsTaggableOn::TagsHelper

  def category_options(chosen_category = nil)
    s = ''
    Category.all.each do |cat|
      s << "<option value='#{cat.id}' data-img-src='' #{'selected' if cat == chosen_category}>#{cat.name}</option>"
    end
    s.html_safe
  end

end
