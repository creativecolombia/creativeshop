json.extract! profile, :id, :firstname, :lastname, :location, :status, :picture, :created_at, :updated_at
json.url profile_url(profile, format: :json)