source 'https://rubygems.org'

gem 'rails', '~> 5.0.0', '>= 5.0.0.1'
#Database
gem 'pg', '~> 0.18'
gem 'pg_search'
#Server
gem 'puma', '~> 3.0'

# Development gems
group :development do
  gem 'rails-erd' #For Model Diagram Generator  bundle exec rake erd
  gem 'railroady' #For Model Diagram Generator  bundle exec rake diagram:all_with_engines
  gem 'web-console'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'rspec-rails'
end


group :development, :test do
  gem 'mysql2'
  gem 'byebug', platform: :mri
end


# Assets
gem 'sass-rails'
gem 'coffee-rails'
gem 'compass-rails'
gem 'haml'
gem 'haml-rails'

gem 'paper_trail'


#FOR improve loadspeed in views. SLIM
#gem 'slim'
#gem 'slim-rails'
#gem 'Haml2Slim'

gem 'jquery-rails'
gem 'uglifier'
gem 'sprockets'
#gem 'bootstrap'
gem 'twitter-bootstrap-rails'
gem 'jbuilder'
gem 'simple_form'
gem 'font-awesome-rails'
gem 'chosen-rails'
gem 'gravatar_image_tag'
gem 'chartkick' #Charts in one line
gem 'auto_html' #todo
gem 'active_link_to' #todo
#gem 'responders'
gem 'payola-payments'
gem 'stripe'
#gem 'stripe-rails'

#model Feature GEMS
gem 'ancestry'
gem 'public_activity' #Log activity
gem 'acts-as-taggable-on', github: 'F3pix/acts-as-taggable-on'
gem 'acts_as_commentable_with_threading' #todo
gem 'acts_as_follower' #review if is needed todo
gem 'acts_as_votable', '~> 0.10.0' #todo for comms
gem 'acts_as_list' #todo
gem 'paranoia', '~> 2.2'
#gem 'acts_as_category', github: 'funkensturm/acts_as_category'

#gem 'merit' #reputation engine todo maybe posible incompatilibility with searchkick

#gem 'punching_bag' TODO review usability
#gem 'audited', '~> 4.3' #log all changes in models. todo

#Money and Currency Gems
gem 'money-rails'
gem 'google_currency'
#gem 'goog_currency' in case google_currency dont work use goog_currency

#Internal Messaging System
gem 'mailboxer'

# Background processing
#gem 'sidekiq'
#gem 'sidekiq-failures'
#gem 'sinatra', :require => nil

# Misc
#gem 'protected_attributes'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
#gem 'active_presenter'
#gem 'auto_strip_attributes', "~> 2.0"
#gem 'countries' #Country gem with iso ISO3166
gem 'country_select'
gem 'rails-i18n' #i18n
gem 'globalize', github: 'globalize/globalize'
gem 'friendly_id-globalize'
gem 'route_translator'
# gem 'activemodel-serializers-xml'
gem 'rack-cors', :require => 'rack/cors' #CORS Feature
gem 'social_shares' #todo
gem 'meta-tags' #todo
gem 'seorel'

#Breadcrumbs
gem 'gretel'

# Pagination
gem 'will_paginate'
gem 'bootstrap-will_paginate'

# RTE
gem 'redactor-rails' #todo

# Authenication
gem 'devise'
gem 'cancan' #todo
gem 'devise-bootstrapped'
gem 'devise-i18n'

#Model Serializer
gem 'active_model_serializers', '~> 0.10.0.rc4'

#Searching Feature
gem 'searchkick' #search inside app with elastic search server

#App model log&tracking
gem 'ahoy_matey' #FOR App Tracking

# Friendly URLs
gem 'friendly_id', '~> 5.1.0' #TODO friendly_id-globalize

# Image and File uploader
gem 'mini_magick'
gem 'carrierwave', github: 'carrierwaveuploader/carrierwave'
gem 'carrierwave-imageoptimizer'

# Sitemap
gem 'sitemap_generator' #todo

# Background processing
#gem 'whenever', '>= 0.8.4', :require => false

# JS Variables
gem 'gon' #todo

# GA
gem 'google-api-client' #todo

# Performance
gem 'fast_blank' #todo
#gem 'jquery-turbolinks'
#gem 'turbolinks'

# Test coverage by Codacy
#gem 'codacy-coverage', :require => false

# documentation
gem 'annotate' #todo

# fake data
gem 'faker' #todo

# Caching
gem 'dalli' #todo implement

#Pushing Notifications
#gem 'rpush' #todo

#ENV variables
gem 'figaro'

#deployment

gem 'capistrano'
gem 'capistrano-rails'
#gem 'capistrano-passenger'
gem 'capistrano-rbenv'
gem 'capistrano-rails-db'
gem 'capistrano3-nginx', '~> 2.0'


#TO IMPLEMENT
gem 'redis', '~> 3.0'

#gem 'sinatra', github: 'sinatra'



#Pushing notifications
#gem 'ruby-push-notifications'

#Geo Ip and google maps
#gem 'geo_pattern'
#gem 'geocoder'
#gem 'gmaps4rails'

#FOR eccomerce
#gem 'credit_card_validations'
#gem 'activemerchant'
#gem 'monetize'
#gem 'money'
#gem 'eu_central_bank'

#FOR API
#gem 'grape-swagger'
#gem 'documentation', '~> 1.0.0'
#gem 'apipie-rails'

#gem 'staccato'
#gem 'impressionist'
#gem 'activerecord-reputation-system'

#gem 'unread'
#gem 'textacular', '~> 3.0'

#search in app
#gem 'search_cop' EXCELENT
#gem "searchjoy"

#gem 'blazer'
#gem 'pghero'
#gem 'chewy'