=begin
Payola.configure do |config|
  # Example subscription:
  # 
  # config.subscribe 'payola.package.sale.finished' do |sale|
  #   EmailSender.send_an_email(sale.email)
  # end
  # 
  # In addition to any event that Stripe sends, you can subscribe
  # to the following special payola events:
  #
  #  - payola.<sellable class>.sale.finished
  #  - payola.<sellable class>.sale.refunded
  #  - payola.<sellable class>.sale.failed
  #
  # These events consume a Payola::Sale, not a Stripe::Event
  #
  # Example charge verifier:
  #
  # config.charge_verifier = lambda do |sale|
  #   raise "Nope!" if sale.email.includes?('yahoo.com')
  # end

  # Keep this subscription unless you want to disable refund handling
  config.subscribe 'charge.refunded' do |event|
    sale = Payola::Sale.find_by(stripe_id: event.data.object.id)
    sale.refund! unless sale.refunded?
  end
end
=end
Payola.configure do |payola|

  payola.secret_key = Rails.application.secrets.stripe_secret_key
  payola.publishable_key = Rails.application.secrets.stripe_publishable_key

  # payola.default_currency = 'gbp'

  payola.subscribe 'payola.book.sale.finished' do |sale|
    YourCustomMailer.receipt(sale.guid).deliver
  end

  payola.subscribe 'payola.book.sale.failed' do |sale|
    YourCustomMailer.admin_failed(sale.guid).deliver
  end

  payola.subscribe 'payola.book.sale.refunded' do |sale|
    YourCustomMailer.admin_refunded(sale.guid).deliver
  end
end