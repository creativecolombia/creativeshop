require 'rails_helper'

RSpec.describe "incoterms/show", type: :view do
  before(:each) do
    @incoterm = assign(:incoterm, Incoterm.create!(
      :smalltag => "Smalltag",
      :name => "Name",
      :description => "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Smalltag/)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Description/)
  end
end
