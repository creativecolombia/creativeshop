Rails.application.routes.draw do


  mount Payola::Engine => '/payola', as: :payola
  localized do
    #devise
    devise_for :users, :controllers => { :registrations => 'registrations' }
    devise_for :admin, :controllers => { :registrations => 'adminregistrations' }
    root 'shop/catalogs#index'

    #shop
    namespace :shop do
      root 'catalogs#index'

      get 'contact',to: 'static#contact'
      get 'testimonials',to: 'static#testimonials',as: 'testimonials'
      get 'about_us',to: 'static#aboutus',as: 'aboutus'

      resources :catalogs, only: [:index,:show]
      resources :categories, only: [:index,:show]
      resources :products, only: [:index,:show]
      resources :offers, only: [:index,:show]
      resources :brands, only: [:index,:show]

    end

    #panel
    namespace :panel do
      root 'static#home'
      get 'sell',to: 'static#sell',as: 'sell'
      get 'buy',to: 'static#buy',as: 'buy'

      resources :profiles, only: [:index,:show]
      resources :orders, only: [:index,:show]
      resources :invoices, only: [:index,:show]
      resources :carts, only: [:index,:show]

      resources :brands
      resources :modells, :path => 'models'
      resources :offers

    end

    #admin
    namespace :admin do
      root 'static#home'

      resources :categorymodels
      resources :modells, :path => 'models'
      resources :activities
      resources :profiles
      resources :brands
      resources :apps, :path => 'app'
      resources :categories
      resources :offers
      resources :incoterms
      resources :conditions


    end

  end


  #TO REVIEW or delete...
  scope '(:locale)', locale: /en|fr|es|de/ do

    resources :users, only: [:index]

    resources :conversations, only: [:index, :show, :destroy] do
      member do
        post :reply
        post :restore
        post :mark_as_read
      end
      collection do
        delete :empty_trash
      end
    end
    resources :messages, only: [:new, :create]

    #get 'shop/index'
    #get 'shop/category/:id', to: 'shop#category', as: 'shop_category'


  end

  namespace :api ,constraints: { format: 'json' } do
    namespace :v1 do
      resources :categories
    end
  end
end


# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
