class ModellSerializer < ActiveModel::Serializer
  attributes :id, :name, :year, :factorypics, :enabled
  has_one :brand
end
