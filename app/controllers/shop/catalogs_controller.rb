class Shop::CatalogsController < ShopController
  before_action :set_category, only: [:show]
  skip_before_action :authenticate_user!, only: [:index,:show],raise: false

  def index
    @categories=Category.find_by_main(true)
    @products=Modell.all
    @brands=Brand.all
  end

  def show
  end
end

private
# Use callbacks to share common setup or constraints between actions.
def set_category
  @category = Category.friendly.find(params[:id])
end