require 'rails_helper'

RSpec.describe "categorymodels/edit", type: :view do
  before(:each) do
    @categorymodel = assign(:categorymodel, Categorymodel.create!(
      :show => nil,
      :modell => nil
    ))
  end

  it "renders the edit categorymodel form" do
    render

    assert_select "form[action=?][method=?]", categorymodel_path(@categorymodel), "post" do

      assert_select "input#categorymodel_category_id[name=?]", "categorymodel[category_id]"

      assert_select "input#categorymodel_modell_id[name=?]", "categorymodel[modell_id]"
    end
  end
end
