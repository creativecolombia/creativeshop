json.extract! condition, :id, :name, :created_at, :updated_at
json.url admin_condition_url(condition, format: :json)