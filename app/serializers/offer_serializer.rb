class OfferSerializer < ActiveModel::Serializer
  attributes :id, :serialnumber, :machinenumber, :agedate, :images, :description, :avability, :price_cents, :enabled
  has_one :condition
  has_one :incoterm
  has_one :modell
end
