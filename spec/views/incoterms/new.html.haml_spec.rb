require 'rails_helper'

RSpec.describe "incoterms/new", type: :view do
  before(:each) do
    assign(:incoterm, Incoterm.new(
      :smalltag => "MyString",
      :name => "MyString",
      :description => "MyString"
    ))
  end

  it "renders new incoterm form" do
    render

    assert_select "form[action=?][method=?]", incoterms_path, "post" do

      assert_select "input#incoterm_smalltag[name=?]", "incoterm[smalltag]"

      assert_select "input#incoterm_name[name=?]", "incoterm[name]"

      assert_select "input#incoterm_description[name=?]", "incoterm[description]"
    end
  end
end
