require 'rails_helper'

RSpec.describe "modells/edit", type: :view do
  before(:each) do
    @modell = assign(:modell, Modell.create!(
      :name => "MyString",
      :factorypics => "",
      :enabled => false,
      :brand => nil
    ))
  end

  it "renders the edit modell form" do
    render

    assert_select "form[action=?][method=?]", modell_path(@modell), "post" do

      assert_select "input#modell_name[name=?]", "modell[name]"

      assert_select "input#modell_factorypics[name=?]", "modell[factorypics]"

      assert_select "input#modell_enabled[name=?]", "modell[enabled]"

      assert_select "input#modell_brand_id[name=?]", "modell[brand_id]"
    end
  end
end
