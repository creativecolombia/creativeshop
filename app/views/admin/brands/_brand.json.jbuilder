json.extract! brand, :id, :name, :sdescription, :description, :logo, :metatitle, :enable, :created_at, :updated_at
json.url admin_brand_url(brand, format: :json)