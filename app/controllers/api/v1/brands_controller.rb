class Api::V1::CategoriesController < ApplicationController
  #respond_to :json
  before_action :set_brand, only: [:show, :edit, :update, :destroy]
  #skip_before_action :authenticate_user!, only: [:index,:show]

  # GET /brands
  def index
    @brand = Brand.all
    render json: @brand
  end


  # GET /brands/1
  def show
    render json: @brand
  end

  # POST /brands
  def create
    @brand = Brand.new(brand_params)
   if @brand.save
      render json: :created, location: @brand
    else
      render json: @brand.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /brands/1
  def update
      if @brand.update(brand_params)
        render json: :ok, location: @brand
      else
        render json: @brand.errors, status: :unprocessable_entity
      end
    end
  end

  # DELETE /brands/1
  def destroy
    @brand.destroy
    render json: :no_content
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_brand
    @brand = Brand.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def brand_params
    params.require(:brand).permit(:name, :sdescription, :description, :logo, :metatitle, :enable)
  end
