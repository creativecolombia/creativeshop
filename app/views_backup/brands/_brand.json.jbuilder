json.extract! brand, :id, :name, :sdescription, :description, :logo, :metatitle, :enable, :created_at, :updated_at
json.url brand_url(brand, format: :json)