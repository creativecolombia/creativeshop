class CreateCategorymodels < ActiveRecord::Migration[5.0]
  def change
    create_table :categorymodels do |t|
      t.references :category, foreign_key: true
      t.references :modell, foreign_key: true

      t.timestamps
    end
  end
end
