require "rails_helper"

RSpec.describe IncotermsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/incoterms").to route_to("incoterms#index")
    end

    it "routes to #new" do
      expect(:get => "/incoterms/new").to route_to("incoterms#new")
    end

    it "routes to #show" do
      expect(:get => "/incoterms/1").to route_to("incoterms#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/incoterms/1/edit").to route_to("incoterms#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/incoterms").to route_to("incoterms#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/incoterms/1").to route_to("incoterms#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/incoterms/1").to route_to("incoterms#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/incoterms/1").to route_to("incoterms#destroy", :id => "1")
    end

  end
end
