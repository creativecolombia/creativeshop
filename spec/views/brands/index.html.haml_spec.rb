require 'rails_helper'

RSpec.describe "brands/index", type: :view do
  before(:each) do
    assign(:brands, [
      Brand.create!(
        :name => "Name",
        :sdescription => "Sdescription",
        :description => "Description",
        :logo => "Logo",
        :metatitle => "Metatitle",
        :enable => false
      ),
      Brand.create!(
        :name => "Name",
        :sdescription => "Sdescription",
        :description => "Description",
        :logo => "Logo",
        :metatitle => "Metatitle",
        :enable => false
      )
    ])
  end

  it "renders a list of brands" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Sdescription".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Logo".to_s, :count => 2
    assert_select "tr>td", :text => "Metatitle".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
