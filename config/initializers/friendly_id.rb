module FriendlyId
  module Globalize

    def should_generate_new_friendly_id?
      regenerated_keys = %w( name title login)
      translation_for(::Globalize.locale).send(friendly_id_config.slug_column).blank?|| (self.changes.keys & regenerated_keys).present?
    end

  end
end