crumb :root do
  link  'Home', root_path
end


#SHOP
crumb :catalogs do
  link t('activerecord.models.app'), shop_catalogs_path
  parent :root
end

crumb :products do
  link t('activerecord.models.modell'), shop_products_path
  parent :root
end
crumb :shopcategories do
  link  t('activerecord.models.category'), shop_categories_path
  parent :root
end
crumb :product do |product|
  link product.name, shop_product_path(product)
  parent :products
end
crumb :shopcategory do |cat|
  link cat.name, shop_category_path(cat)
  parent :shopcategories
end
crumb :shopbrand do |brand|
  link brand.name, shop_brand_path(brand)
  parent :catalogs
end



#Admin

crumb :admin do
  link 'Admin', admin_root_path
end


crumb :app do
  link t('activerecord.models.app'), admin_app_path(App.first)
end

crumb :categories do
  link t('activerecord.models.category'), admin_categories_path
  parent :admin
end

crumb :profiles do
  link t('activerecord.models.profile'), admin_profiles_path
  parent :admin
end


crumb :brands do
  link t('activerecord.models.brand'), admin_brands_path
  parent :admin
end

crumb :conditions do
  link t('activerecord.models.condition'), admin_conditions_path
  parent :admin
end

crumb :models do
  link t('activerecord.models.modell'), admin_modells_path
  parent :admin
end

crumb :incoterms do
  link t('activerecord.models.incoterm'), admin_incoterms_path
  parent :admin
end

crumb :offers do
  link t('activerecord.models.offers'), admin_offers_path
  parent :admin
end

crumb :show do |category|
  link category.name, admin_categories_path(category)
  parent :categories
end

crumb :profile do |profile|
  link profile.firstname, admin_profiles_path(profile)
  parent :profiles
end

crumb :brand do |brand|
  link brand.name, admin_brands_path(brand)
  parent :brands
end

crumb :offer do |offer|
  link offer.id, admin_offers_path(offer)
  parent :admin
end

crumb :category do |category|
  link category.name, admin_category_path(category)
  parent :categories
end

crumb :modell do |model|
  link model.name, admin_modells_path(model)
  parent :admin
end





# crumb :projects do
#   link "Projects", projects_path
# end

# crumb :project do |project|
#   link project.name, project_path(project)
#   parent :projects
# end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).