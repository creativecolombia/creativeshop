json.extract! profile, :id, :firstname, :lastname, :location, :status, :picture, :created_at, :updated_at
json.url admin_profile_url(profile, format: :json)