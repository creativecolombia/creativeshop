class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  acts_as_messageable
  #acts_as_tagger

  has_one :profile

  #has_many :activities TODO implement

  def name
    email
  end

  def mailboxer_email(object)
    email
  end
end
