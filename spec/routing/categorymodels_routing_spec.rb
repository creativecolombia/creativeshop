require "rails_helper"

RSpec.describe CategorymodelsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/categorymodels").to route_to("categorymodels#index")
    end

    it "routes to #new" do
      expect(:get => "/categorymodels/new").to route_to("categorymodels#new")
    end

    it "routes to #show" do
      expect(:get => "/categorymodels/1").to route_to("categorymodels#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/categorymodels/1/edit").to route_to("categorymodels#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/categorymodels").to route_to("categorymodels#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/categorymodels/1").to route_to("categorymodels#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/categorymodels/1").to route_to("categorymodels#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/categorymodels/1").to route_to("categorymodels#destroy", :id => "1")
    end

  end
end
