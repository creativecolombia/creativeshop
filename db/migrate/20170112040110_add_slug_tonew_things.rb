class AddSlugTonewThings < ActiveRecord::Migration[5.0]
  def change
    add_column :incoterms, :slug, :string
    add_index  :incoterms, :slug
  end
end
