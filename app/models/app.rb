class App < ApplicationRecord
  include PublicActivity::Model
  extend FriendlyId

  tracked owner:  -> (controller, model ) {controller && controller.current_user}
  mount_uploader :avatar, AvatarUploader
  friendly_id :slug_candidates, use: [:slugged, :finders]

  def slug_candidates
    [
        :name,
        [:name, :id]
    ]
  end
  def should_generate_new_friendly_id?
    name_changed? || super
  end
end
