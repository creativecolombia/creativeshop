class Fixmigrationtomodel < ActiveRecord::Migration[5.0]
  def change
    remove_column :modells, :country_id, :integer
    add_column :modells, :country_code, :integer
  end
end
