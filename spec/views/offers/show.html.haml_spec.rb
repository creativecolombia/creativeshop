require 'rails_helper'

RSpec.describe "offers/show", type: :view do
  before(:each) do
    @offer = assign(:offer, Offer.create!(
      :serialnumber => "Serialnumber",
      :machinenumber => "Machinenumber",
      :condition => nil,
      :incoterm => nil,
      :images => "",
      :description => "Description",
      :avability => false,
      :price_cents => "Price Cents",
      :modell => nil,
      :enabled => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Serialnumber/)
    expect(rendered).to match(/Machinenumber/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/Description/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/Price Cents/)
    expect(rendered).to match(//)
    expect(rendered).to match(/false/)
  end
end
