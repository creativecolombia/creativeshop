class CreateModells < ActiveRecord::Migration[5.0]
  def change
    create_table :modells do |t|
      t.string :name
      t.date :year
      t.json :factorypics
      t.boolean :enabled
      t.references :brand, foreign_key: true

      t.timestamps
    end
  end
end
