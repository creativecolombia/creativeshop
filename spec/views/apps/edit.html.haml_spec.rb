require 'rails_helper'

RSpec.describe "apps/edit", type: :view do
  before(:each) do
    @app = assign(:app, App.create!(
      :name => "MyString",
      :description => "MyString",
      :avatar => "MyString",
      :allownew => false
    ))
  end

  it "renders the edit app form" do
    render

    assert_select "form[action=?][method=?]", app_path(@app), "post" do

      assert_select "input#app_name[name=?]", "app[name]"

      assert_select "input#app_description[name=?]", "app[description]"

      assert_select "input#app_avatar[name=?]", "app[avatar]"

      assert_select "input#app_allownew[name=?]", "app[allownew]"
    end
  end
end
