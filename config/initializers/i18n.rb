
Rails.application.config.i18n.default_locale = :en
Rails.application.config.i18n.available_locales = [:en,:es,:fr,:de,:it]
#config.i18n.fallbacks = false
#config.i18n.fallbacks = {'de' => 'en','es' => 'en','fr' => 'en'}
languages={:en => [:fr, :de, :es, :it],
           :de => [:en, :fr, :es, :it],
           :es => [:en, :fr, :de, :it],
           :fr => [:en, :es, :de, :it],
           :it => [:en, :fr, :es, :de]}
Rails.application.config.i18n.fallbacks = languages
Globalize.fallbacks = languages