# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
def image(name)
  Pathname.new(Rails.root.join('db', 'seeds_data',name )).open
end

application= App.create(name: 'Printerspart', description:'',  allownew: true, avatar: image('logo.png'))

categorymain= Category.create(name:'Home',description: '', main: true)
new= Category.create(name:'New',parent_id: categorymain.id)
new.set_translations(es: {name: "Nuevo"},
                     fr: {name: "Nouve"})
new.save

=begin

newused=[:new, :used]
subs=[]
subcats=[]


newused.each do |cat|
  a_cat= Category.create(name:cat,parent_id: home.id)
  subs.each do |sub|
    a_sub=Category.create(name: sub,parent_id: a_cat.id)
    subcats.each do |subcat|
      Category.create(name: subcat,parent_id: a_sub.id)
    end
  end
end
=end

used= Category.create(name:'Used',parent_id: categorymain.id)


#NEW
prepressnew= Category.create(name:'PrePress',parent_id: new.id)

a=Category.create(name:'Spareparts',parent_id: prepressnew.id)
b=Category.create(name:'Accessories',parent_id: prepressnew.id)
c=Category.create(name:'Consumables',parent_id: prepressnew.id)
d=Category.create(name:'Bazar',parent_id: prepressnew.id)

printingnew= Category.create(name:'Printing',parent_id: new.id)

e=Category.create(name:'Spareparts',parent_id: printingnew.id)
f=Category.create(name:'Accessories',parent_id: printingnew.id)
g=Category.create(name:'Consumables',parent_id: printingnew.id)
h=Category.create(name:'Bazar',parent_id: printingnew.id)


bookbindingingnew= Category.create(name:'Bookbinding',parent_id: new.id)

i=Category.create(name:'Spareparts',parent_id: bookbindingingnew.id)
j=Category.create(name:'Accessories',parent_id: bookbindingingnew.id)
k=Category.create(name:'Consumables',parent_id: bookbindingingnew.id)
l=Category.create(name:'Bazar',parent_id: bookbindingingnew.id)

postpressnew= Category.create(name:'PostPress',parent_id: new.id)

m=Category.create(name:'Spareparts',parent_id: postpressnew.id)
n=Category.create(name:'Accessories',parent_id: postpressnew.id)
o=Category.create(name:'Consumables',parent_id: postpressnew.id)
p=Category.create(name:'Bazar',parent_id: postpressnew.id)

bazarnew= Category.create(name:'Bazar',parent_id: new.id)

q=Category.create(name:'Spareparts',parent_id: bazarnew.id)
r=Category.create(name:'Accessories',parent_id: bazarnew.id)
s=Category.create(name:'Consumables',parent_id: bazarnew.id)
t=Category.create(name:'Bazar',parent_id: bazarnew.id)
#USED
prepressused= Category.create(name:'PrePress',parent_id: used.id)

u=Category.create(name:'Spareparts',parent_id: prepressused.id)
v=Category.create(name:'Accessories',parent_id: prepressused.id)
x=Category.create(name:'Consumables',parent_id: prepressused.id)
y=Category.create(name:'Bazar',parent_id: prepressused.id)

printingused= Category.create(name:'Printing',parent_id: used.id)

z=Category.create(name:'Spareparts',parent_id: printingused.id)
aa=Category.create(name:'Accessories',parent_id: printingused.id)
bb=Category.create(name:'Consumables',parent_id: printingused.id)
cc=Category.create(name:'Bazar',parent_id: printingused.id)


bookbindingused= Category.create(name:'Bookbinding',parent_id: used.id)

dd=Category.create(name:'Spareparts',parent_id: bookbindingused.id)
ee=Category.create(name:'Accessories',parent_id: bookbindingused.id)
ff=Category.create(name:'Consumables',parent_id: bookbindingused.id)
gg=Category.create(name:'Bazar',parent_id: bookbindingused.id)

postpressused= Category.create(name:'PostPress',parent_id: used.id)

hh=Category.create(name:'Spareparts',parent_id: postpressused.id)
ii=Category.create(name:'Accessories',parent_id: postpressused.id)
jj=Category.create(name:'Consumables',parent_id: postpressused.id)
Category.create(name:'Bazar',parent_id: postpressused.id)

bazarused= Category.create(name:'Bazar',parent_id: used.id)

Category.create(name:'Spareparts',parent_id: bazarused.id)
Category.create(name:'Accessories',parent_id: bazarused.id)
Category.create(name:'Consumables',parent_id: bazarused.id)
Category.create(name:'Bazar',parent_id: bazarused.id)



new=Condition.create(name: 'New')
lnew=Condition.create(name: 'Like new')
vgood=Condition.create(name: 'Very good')
good=Condition.create(name: 'Good')
bad=Condition.create(name: 'Bad')
vbad=Condition.create(name: 'Very bad')

exw = Incoterm.create(smalltag: 'EXW', name: 'Ex Works', description: 'The exporter delivers the products ...')






#TODO DELETE just for testing prop.
afmp94 = User.create! :email => 'afmp.94@hotmail.com', :password => 'password', :password_confirmation => 'password'
Profile.create(firstname: 'Andres Felipe', lastname: 'Murcia',picture: image('profiles/afmp.png'), user: afmp94)
corin = User.create! :email => 'corin.langosch@netskin.com', :password => 'password', :password_confirmation => 'password'
Profile.create(firstname: 'Corin', lastname: 'Langosch', user: corin)
horst = User.create! :email => 'horst@roepa.com', :password => 'password', :password_confirmation => 'password'
Profile.create(firstname: 'Horst', lastname: 'Roesinger', user: horst)
lali = User.create! :email => 'laurasofiachaves@gmail.com', :password => 'password', :password_confirmation => 'password'
Profile.create(firstname: 'Laura', lastname: 'Chaves', picture: image('profiles/lali.png'), user: lali)

adminafmp94 = Admin.create! :email => 'afmp.94@hotmail.com', :password => 'password', :password_confirmation => 'password'
Profile.create(firstname: 'Andres Felipe', lastname: 'Murcia',picture: image('profiles/afmp.png'), admin: adminafmp94)
admincorin = Admin.create! :email => 'corin.langosch@netskin.com', :password => 'password', :password_confirmation => 'password'
Profile.create(firstname: 'Corin', lastname: 'Langosch', admin: admincorin)
adminhorst = Admin.create! :email => 'horst@roepa.com', :password => 'password', :password_confirmation => 'password'
Profile.create(firstname: 'Horst', lastname: 'Roesinger', admin: adminhorst)
adminlali = Admin.create! :email => 'laurasofiachaves@gmail.com', :password => 'password', :password_confirmation => 'password'
Profile.create(firstname: 'Laura', lastname: 'Chaves',picture: image('profiles/lali.png') , admin: adminlali)
#TODO delete





#TODO DELETE just for testing prop.
#Brands
roland = Brand.create(
    name:'Roland',
    description:'Roland DGAs best-selling line of large-format inkjet printers and integrated printer/cutters, vinyl cutters, engravers, 3D modeling solutions, photo impact printers ',
    logo: image('brands/roland/logo.png'),
    enable: true,
    metatitle: 'Roland'
)

kodak = Brand.create(
    name:'Kodak',
    description:'Kodak is a technology company focused on imaging. We provide – directly and through partnerships – innovative hardware, software, consumables and ...',
    logo: image('brands/kodak/logo.png'),
    enable: true,
    metatitle: 'Kodak'
)
heidelberg = Brand.create(
    name:'Heidelgerg',
    description:'High Quality Printing Machines',
    logo: image('brands/heidelberg/logo.png'),
    enable: true,
    metatitle: 'Heilderberg'
)




#Models
soljet=Modell.create(
    name: 'Soljet Pro 4x XR-640' ,
    enabled: true,
    country_code: 1,
    brand_id: 1
)

truevis=Modell.create(
    name: 'TrueVIS SG Series Printer/Cutters' ,
    enabled: true,
    country_code: 1,
    brand_id: 1
)


magnus400= Modell.create(
    name: 'Magnus 400' ,
    enabled: true,
    country_code: 1,

brand_id: kodak.id
)
magnus800= Modell.create(
    name: 'Magnus 800' ,
    enabled: true,
    country_code: 1,

brand_id: kodak.id
)


#Category - adding models
Categorymodel.create(
    category_id:  aa.id,
    modell_id: magnus400.id
)
Categorymodel.create(
    category_id:  aa.id,
    modell_id: magnus800.id
)
Categorymodel.create(
    category_id:  aa.id,
    modell_id: truevis.id
)
Categorymodel.create(
    category_id:  aa.id,
    modell_id: magnus400.id
)
Categorymodel.create(
    category_id:  aa.id,
    modell_id: soljet.id
)
Categorymodel.create(
    category_id:  aa.id,
    modell_id: soljet.id
)

Categorymodel.create(
    category_id:  b.id,
    modell_id: magnus800.id
)
Categorymodel.create(
    category_id:  cc.id,
    modell_id: truevis.id
)
Categorymodel.create(
    category_id:  d.id,
    modell_id: magnus400.id
)
Categorymodel.create(
    category_id:  a.id,
    modell_id: soljet.id
)
Categorymodel.create(
    category_id:  c.id,
    modell_id: soljet.id
)



#Offers
Offer.create(
    serialnumber: '837397362376',
    machinenumber: 'ASEF9837',
    condition_id: lnew.id,
    description: 'a beautiful machine  ',
    avability: 'yes',
    price_cents: '93459,99',
    modell_id: soljet.id,
    enabled: true
)

Offer.create(
    serialnumber: '172376',
    machinenumber: 'AS12323',
    condition_id: vbad.id,
    description: 'a beautiful machine but it does not work. ',
    avability: 'yes',
    price_cents: '9459,99',
    modell_id: soljet.id,
    enabled: true
)

Offer.create(
    serialnumber: '153721234376',
    machinenumber: 'AS12323',
    condition_id: vgood.id,
    description: 'a beautiful machine but it does not work. ',
    avability: 'yes',
    price_cents: '12499,99',
    modell_id: truevis.id,
    enabled: true
)
Offer.create(
    serialnumber: '5321721235376',
    machinenumber: 'AS12323',
    condition_id: good.id,
    description: 'a beautiful machine but it does not work. ',
    avability: 'yes',
    price_cents: '9129,99',
    modell_id: truevis.id,
    enabled: true
)

Offer.create(
    serialnumber: '1723523712356',
    machinenumber: 'AS12323',
    condition_id: vgood.id,
    description: 'a beautiful machine but it does not work. ',
    avability: 'yes',
    price_cents: '9429,99',
    modell_id: magnus400.id,
    enabled: true
)
Offer.create(
    serialnumber: '172523712536',
    machinenumber: 'AS12323',
    condition_id: good.id,
    description: 'a beautiful machine but it does not work. ',
    avability: 'yes',
    price_cents: '3299,99',
    modell_id: magnus400.id,
    enabled: true
)
Offer.create(
    serialnumber: '1235172325376',
    machinenumber: 'AS12323',
    condition_id: bad.id,
    description: 'a beautiful machine but it does not work. ',
    avability: 'yes',
    price_cents: '2199,99',
    modell_id: magnus800.id,
    enabled: true
)
