require 'rails_helper'

RSpec.describe "incoterms/edit", type: :view do
  before(:each) do
    @incoterm = assign(:incoterm, Incoterm.create!(
      :smalltag => "MyString",
      :name => "MyString",
      :description => "MyString"
    ))
  end

  it "renders the edit incoterm form" do
    render

    assert_select "form[action=?][method=?]", incoterm_path(@incoterm), "post" do

      assert_select "input#incoterm_smalltag[name=?]", "incoterm[smalltag]"

      assert_select "input#incoterm_name[name=?]", "incoterm[name]"

      assert_select "input#incoterm_description[name=?]", "incoterm[description]"
    end
  end
end
