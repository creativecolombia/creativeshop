json.extract! incoterm, :id, :smalltag, :name, :description, :created_at, :updated_at
json.url admin_incoterm_url(incoterm, format: :json)